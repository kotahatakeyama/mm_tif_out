
var resizeHandler = function(){
	
	var w = $(window).width();
	var h = $(window).height();

	if(!('innerWidth' in window)) window.innerWidth = w;

	if(window.innerWidth > 767){

		var minH=490;
		var maxH=750;
		var headerHeight = 120;
	
		if(w<1280){
			$("#Mainvis_bg").css({width: "100%"});
			}else{
			$("#Mainvis_bg").css({width: "100%"});
		 }
	
		if(h<minH){
			$('#Mainvis_bg').css({height: minH-headerHeight});	
		}else if(h>maxH){
			$('#Mainvis_bg').css({height: maxH-headerHeight});	
		}else{
			$('#Mainvis_bg').css({height: h-headerHeight});
		}
		
	}else{
		
		$('#Mainvis_bg').css({ width:'', height:'' });	
		
	}
	
	//$(".imgstyle_01").css("display","none");

};

$(document).ready(resizeHandler);
$(window).resize(resizeHandler);

